const express = require('express');
//everything here is required for the authentication setup
const cookieParser = require("cookie-parser");
const jwt = require("express-jwt");
const jsonwebtoken = require("jsonwebtoken");
const morgan = require('morgan'); // logging middleware
const expireTime = 1800;
const jwtSecret = '6xvL4xkAAbG49hcXf5GIYSvkDICiUAR6EdR5dLdwW7hMzUjjMUe9t6M5kSAYxsvX';

// Authorization error
const authErrorObj = { errors: [{ 'param': 'Server', 'msg': 'Authorization error' }] };

const OfficerDao = require("./officer_dao");
const CounterDao = require("./counter_dao.js");
const ServiceDao = require("./service_dao.js");
const ConfigurationDao = require("./configuration_dao.js");

const PORT = 3001;

app = new express();

// Set-up logging
app.use(morgan('tiny'));

app.use(express.json());


app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

module.exports =  app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}/`));


app.post('/api/login', (req, res) => {
  const officerAccount = req.body;
  if (!officerAccount) {
    res.status(400).end();
  } else {
    OfficerDao.getOfficerByEmail(officerAccount.email)
      .then((user) => {
        if (user === undefined || user.password != officerAccount.password) {
          res.status(200).json({ error_no: -1, error_info: "Email or password is wrong." })
        } else {
          //AUTHENTICATION SUCCESS
          //const token = jsonwebtoken.sign({ user: user.officerID }, jwtSecret, {expiresIn: expireTime});
          //res.cookie('token', token, { httpOnly: true, sameSite: true, maxAge: 1000 * expireTime });
          res.status(200).json({ user: { name: user.name, email: user.email, isManager: user.isManager }, error_no: 0, error_info: "Login successfully." });
        }
      }).catch(
        // Delay response when wrong user/pass is sent to avoid fast guessing attempts
        (err) => {
          new Promise((resolve) => { setTimeout(resolve, 1000) }).then(() => res.status(401).json(authErrorObj))
        }
      );
  }
})

//----------------------COOKIE--------------------------
//TODO: to be tested (if needed)
/*
app.use(cookieParser());


// For the rest of the code, all APIs require authentication
app.use(
  jwt({
    secret: jwtSecret,
    getToken: req => req.cookies.token
  })
);
*/



/**Create the officer account*/
app.post('/api/officers', (req, res) => {
  const user = req.user && req.user.user; // Needed to check authorization TODO: check if is admin
  const officerAccount = req.body;
  if (!officerAccount) {
    res.status(400).end();
  } else if (false || (user && user.isManager)) { // TODO: Check here if the user is a manager
    res.status(401).json(authErrorObj);
  } else {
    OfficerDao.getOfficerByEmail(officerAccount.email)
      .then((user) => {
        console.log(user);
        if (user === undefined) { // i can continue only if there are not other people with this email
          OfficerDao.createOfficerAccount(officerAccount)
            .then((id) => res.status(201).json({ "id": id }))
            .catch((err) => {
              res.status(500).json({ errors: [{ 'param': 'Server', 'msg': err }], })
            });
        } else {
          res.status(403).end(); // Error to indicate a duplicate email in the database
        }
      })
      .catch((err) => { res.status(500).json({ errors: [{ 'param': 'Server', 'msg': err }], }) });

  }
});

/**Create a service*/
app.post('/api/service', (req, res) => {
  const user = req.user && req.user.user;
  const service = req.body;
  if (!service) {
    res.status(400).end();
  } else if (false || (user && user.isManager)) { // TODO: Check here if the user is a manager
    res.status(401).json(authErrorObj);
  } else {
    ServiceDao.getServiceByDescription(service.description)
      .then((service) => {
        console.log(service);
        if (service === undefined) { // i can continue only if there are not other services with the same description
          ServiceDao.createService(service)
            .then((id) => res.status(201).json({ "id": id }))
            .catch((err) => {
              res.status(500).json({ errors: [{ 'param': 'Server', 'msg': err }], })
            });
        } else {
          res.status(403).end(); // Error to indicate a duplicate description in the database
        }
      })
      .catch((err) => { res.status(500).json({ errors: [{ 'param': 'Server', 'msg': err }], }) });

  }
});

/**Create a configuration*/
app.post('/api/configuration', (req, res) => {
  const user = req.user && req.user.user;
  const configuration = req.body;
  if (!configuration) {
    res.status(400).end();
  } else if (false || (user && user.isManager)) { // TODO: Check here if the user is a manager
    res.status(401).json(authErrorObj);
  } else {
    ConfigurationDao.createConfiguration(configuration)
      .then((configuration) => {
        console.log(configuration);
        ((id) => res.status(201).json({ "id": id }))
      }).catch((err) => {
        res.status(500).json({ errors: [{ 'param': 'Server', 'msg': err }], })
      });
  }
});

//Get all counters in the office
app.get("/api/counters", (req, res) => {
  CounterDao.getCounters().then((counters) => {
    res.json(counters);
  }).catch((err) => res.status(500).json({ errors: [{ msg: err }] }))
})

//Get all services offered
app.get("/api/services", (req, res) => {
  ServiceDao.getServicesAndDescriptions().then((services) => {
    res.json(services);
  }).catch((err) => res.status(500).json({ errors: [{ msg: err }] }))
})

//Get all already registered configurations
app.get("/api/configurations", (req, res) => {
  ConfigurationDao.getConfigurations().then((configs) => {
    res.json(configs);
  }).catch((err) => res.status(500).json({ errors: [{ msg: err }] }))
})

//Get the serving time
app.get("/api/servingTime", (req, res) => {
  ServiceDao.getServingTime().then((servingTime) => {
    res.json(servingTime);
  }).catch((err) => res.status(500).json({ errors: [{ msg: err }] }))
})

//Checks if a given configuration is already in the database, and if it isn't it saves it
app.post("/api/newConfiguration", (req, res) => {
  const configuration = req.body;
  if (!configuration) {
    res.status(400).end();
  } else {
    ConfigurationDao.saveNewConfiguration(configuration)
      .then((configuration) => {
        console.log(configuration);
        ((id) => res.status(201).json({ "id": id }))
      }).catch((err) => {
        res.status(500).json({ errors: [{ 'param': 'Server', 'msg': err }], })
      });
  }
})

//Checks if a given configuration is already in the database, and if it is it deletes it
app.post("/api/noConfiguration", (req, res) => {
  const configuration = req.body;
  if (!configuration) {
    res.status(400).end();
  } else {
    ConfigurationDao.checkNoConfiguration(configuration)
      .then((configuration) => {
        console.log(configuration);
        ((id) => res.status(201).json({ "id": id }))
      }).catch((err) => {
        res.status(500).json({ errors: [{ 'param': 'Server', 'msg': err }], })
      });
  }
})