class Counter {
    constructor (counterId, openingTime, closingTime) {
        this.counterId = counterId;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
    }
}

module.exports = Counter;