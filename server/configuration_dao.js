'use strict';

const db = require('./db');
const Configuration = require('./configuration');

function createConfiguration(row) {
    return new Configuration(row.configurationId, row.counterId, row.serviceId);
}

exports.createConfiguration = function (configuration) {
    return new Promise((resolve, reject) => {
        const sql = 'INSERT INTO Configuration(counterId, serviceId) VALUES(?, ?)';
        let params = [];
        console.log("new Configuration: ", configuration);
        params.push(configuration.counterId, configuration.serviceId);

        if (service)
            db.run(sql, params, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(this.lastID);
                }
            });
    });
}

exports.getConfigurations = function () {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM Configuration";
        db.all(sql, [], (err, rows) => {
            if (err) {
                reject(err)
            } else {
                let configuration = rows.map(row => createConfiguration(row));
                resolve(configuration);
            }
        })
    })
}

exports.saveNewConfiguration = function (configuration) {
    return new Promise((resolve, reject) => {
        const sqlSelect = "SELECT COUNT(*) FROM Configuration WHERE counterId = ? AND serviceId = ?"; //when receiving a new configuration it checks if it already exists in the database
        let params = [];
        params.push(configuration.counterId, configuration.serviceId);
        db.get(sqlSelect, params, (err, row) => {
            if (err) {
                reject(err)
            } else {
                let count = row["COUNT(*)"];
                console.log(count)
                if (count === 0) { //the configuration is saved only if it's new (chance that a manager works again on a counter and then already registered configurations cause problems)
                    const sqlGetMax = "SELECT MAX(configurationId) FROM Configuration";
                    db.get(sqlGetMax, [], (err, row) => {
                        if (err) {
                            reject(err);
                        } else {
                            let maxConfId = 1; //need to get the maximum configuration id to insert a new tuple in the table
                            if (row !== undefined) {
                                maxConfId = row["MAX(configurationId)"] + 1;

                            }
                            const sqlInsert = "INSERT INTO Configuration(configurationId, counterId, serviceId) VALUES(?, ?, ?)";
                            let params = [];
                            params.push(maxConfId, configuration.counterId, configuration.serviceId);
                            db.run(sqlInsert, params, function (err) {
                                if (err) {
                                    reject(err);
                                }
                                else {
                                    resolve(this.lastID);
                                }
                            });
                        }
                    })
                }
            }
        })
    })
}

exports.checkNoConfiguration = function (configuration) {
    return new Promise((resolve, reject) => {
        const sqlSelect = "SELECT COUNT(*) FROM Configuration WHERE counterId = ? AND serviceId = ?"; //reverse of the previous function, checks if the configuration is already present in the database
        let params = [];
        params.push(configuration.counterId, configuration.serviceId);
        db.get(sqlSelect, params, (err, row) => {
            if (err) {
                reject(err)
            } else {
                let count = row["COUNT(*)"];
                if (count !== 0) {//if the configuration is present in the database it must be removed
                    const sqlDelete = "DELETE FROM Configuration WHERE counterId = ? AND serviceId =?";
                    let params = [];
                    params.push(configuration.counterId, configuration.serviceId);
                    db.run(sqlDelete, params, function (err) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(this.lastID);
                        }
                    });
                }
            }
        })
    })
}