class Service {
    constructor (serviceID, description, servingTime) {
        this.serviceID = serviceID;
        this.description = description;
        this.servingTime = servingTime;
    }
}

module.exports = Service;