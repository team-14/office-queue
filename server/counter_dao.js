'use strict';

const db = require('./db');
const Counter = require('./counter');

function createCounter(row) {
    return new Counter(row.counterId, row.openingTime, row.closingTime);
}

exports.createCounter = function (counter) {
    return new Promise((resolve, reject) => {
        const sql = 'INSERT INTO Counter(openingTime, closingTime) VALUES(?, ?)';
        let params = [];
        console.log("new Counter: ", counter);
        params.push(counter.openingTime, counter.closingTime);

        if (counter)
            db.run(sql, params, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(this.lastID);
                }
            });
    });
}

exports.getCounterById = function (counterID) {
    return new Promise((resolve, reject) => {
        const sql = "SELECT * FROM Counter WHERE counterID = ?";
        db.all(sql, [counterID], (err, row) => {
            if (err)
                reject(err);
            else {
                if (row)
                    resolve(createCounter(row));
                else
                    resolve(undefined);
            }
        });
    });
}

exports.getCounters = function () {
    return new Promise((resolve, reject) => {
        let sql = "SELECT counterId FROM Counter";
        db.all(sql, [], (err, rows) => {
            if (err) {
                reject(err)
            } else {
                console.log("a");
                console.log(rows);
                resolve(rows.map(row => row.counterId));
            }
        })
    })
}