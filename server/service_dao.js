'use strict';

const db = require('./db');
const Service = require('./service');

exports.createService = function (service) {
    return new Promise((resolve, reject) => {
        const sql = 'INSERT INTO Service(description, servingTime) VALUES(?, ?)';
        let params = [];
        console.log("new Service: ", service);
        params.push(service.description, service.servingTime);

        if (service)
            db.run(sql, params, function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(this.lastID);
                }
            });
    });
}

exports.getServices = function () {
    return new Promise((resolve, reject) => {
        let sql = "SELECT description FROM Service";
        db.all(sql, [], (err, rows) => {
            if (err) {
                reject(err)
            } else {
                resolve(rows.map(row => row.description));
            }
        })
    })
}

exports.getServingTime = function (description) {
    return new Promise((resolve, reject) => {
        let sql = "SELECT servingTime FROM Service WHERE description = ?";
        db.all(sql, [description], (err, rows) => {
            if (err) {
                reject(err)
            } else {
                resolve(rows);
            }
        })
    })
}

exports.getServiceByDescription = function (desctiption) {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM Service WHERE description = ?";
        db.all(sql, [description], (err, rows) => {
            if (err) {
                reject(err)
            } else {
                resolve(rows);
            }
        })
    })
}

exports.getServicesAndDescriptions = function () {
    return new Promise((resolve, reject) => {
        let sql = "SELECT description, serviceId FROM Service";
        db.all(sql, [], (err, rows) => {
            if (err) {
                reject(err)
            } else {
                resolve(rows);
            }
        })
    })
}