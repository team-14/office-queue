class Shift {
    constructor (shiftID, officerID, counterID, date) {
        this.shiftID = shiftID;
        this.officerID = officerID;
        this.counterID = counterID;
        this.date = date;
    }
}

module.exports = Shift;