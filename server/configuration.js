class Configuration {
    constructor (configurationId, counterId, serviceId) {
        this.configurationId = configurationId;
        this.counterId = counterId;
        this.serviceId = serviceId;
    }
}

module.exports = Configuration;