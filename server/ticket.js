class Ticket {
    constructor (ticketID, openingTime, closingTime, served, counterID, serviceID, ticketNumber) {
        this.ticketID = ticketID;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
        this.served = served;
        this.counterID = counterID;
		this.serviceID = serviceID;
		this.ticketNumber = ticketNumber;
    }
}

module.exports = Ticket;