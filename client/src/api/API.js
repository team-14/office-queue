const baseURL = "http://localhost:3001/api";

//-----------------------AUTHENTICATION---------------------------

async function isAuthenticated() {
    let url = "/user";
    const response = await fetch(baseURL + url);
    const userJson = await response.json();
    if (response.ok) {
        return userJson;
    } else {
        let err = { status: response.status, errObj: userJson };
        throw err;
    }
}


//---------------------OFFICER ACCOUNT CREATION---------------------

async function createOfficerAccount(officerAccount) {  // Deve funzionare anche senza autenticazione
    return new Promise((resolve, reject) => {
        fetch(baseURL + "/officers", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(officerAccount),
        }).then((response) => {
            if (response.ok) {
                resolve(response.json());
            } else {
                response.json()
                    .then((obj) => { reject(obj); })
                    .catch((err) => { reject({ errors: [{ param: "Application", msg: "Cannot parse server response" }] }) });
            }
        }).catch((err) => { reject({ errors: [{ param: "Server", msg: "Cannot communicate" }] }) });
    });
}

async function login(user) {
    return new Promise((resolve, reject) => {
        fetch(baseURL + "/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(user),
        }).then((response) => {
            if (response.ok) {
                resolve(response.json());
            } else {
                response.json()
                    .then((obj) => { reject(obj); })
                    .catch((err) => { reject({ errors: [{ param: "Application", msg: "Cannot parse server response" }] }) });
            }
        }).catch((err) => { reject({ errors: [{ param: "Server", msg: "Cannot communicate" }] }) });
    });
}

//-----------------------GET OF ALL COUNTERS------------------------

async function getCounters() {
    let url = "/counters";
    const response = await fetch(baseURL + url);
    const countersJson = await response.json();
    if (response.ok) {
        return countersJson;
    } else {
        let err = {
            status: response.status, errObj: countersJson
        };
        throw err;
    }
}

//-----------------------GET OF ALL SERVICES------------------------

async function getServices() {
    let url = "/services";
    const response = await fetch(baseURL + url);
    const servicesJson = await response.json();
    if (response.ok) {
        return servicesJson;
    } else {
        let err = {
            status: response.status, errObj: servicesJson
        };
        throw err;
    }
}

//-----------------------GET OF ALL CONFIGURATIONS (COUNTERS + SERVICES)------------------------

async function getConfigurations() {
    let url = "/configurations";
    const response = await fetch(baseURL + url);
    const configurationsJson = await response.json();
    if (response.ok) {
        return configurationsJson;
    } else {
        let err = {
            status: response.status, errObj: configurationsJson
        };
        throw err;
    }
}

//---------------------SERVICE CREATION---------------------

async function createService(service) {  // Deve funzionare anche senza autenticazione
    return new Promise((resolve, reject) => {
        fetch(baseURL + "/service", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(service),
        }).then((response) => {
            if (response.ok) {
                resolve(null);
            } else {
                response.json()
                    .then((obj) => { reject(obj); })
                    .catch((err) => { reject({ errors: [{ param: "Application", msg: "Cannot parse server response" }] }) });
            }
        }).catch((err) => { reject({ errors: [{ param: "Server", msg: "Cannot communicate" }] }) });
    });
}

//-----------------------GET SERVING TIME------------------------

async function getServingTime() {
    let url = "/servingTime";
    const response = await fetch(baseURL + url);
    const servingTimeJson = await response.json();
    if (response.ok) {
        return servingTimeJson;
    } else {
        let err = {
            status: response.status, errObj: servingTimeJson
        };
        throw err;
    }
}

//---------------------CONFIGURATION CREATION---------------------

async function createConfiguration(configuration) {  // Deve funzionare anche senza autenticazione
    return new Promise((resolve, reject) => {
        fetch(baseURL + "/configuration", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(configuration),
        }).then((response) => {
            if (response.ok) {
                resolve(null);
            } else {
                response.json()
                    .then((obj) => { reject(obj); })
                    .catch((err) => { reject({ errors: [{ param: "Application", msg: "Cannot parse server response" }] }) });
            }
        }).catch((err) => { reject({ errors: [{ param: "Server", msg: "Cannot communicate" }] }) });
    });
}

async function saveNewConfiguration(configuration) {
    return new Promise((resolve, reject) => {
        fetch(baseURL + "/newConfiguration", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(configuration),
        }).then((response) => {
            if (response.ok) {
                resolve(null);
            } else {
                response.json()
                    .then((obj) => { reject(obj); })
                    .catch((err) => { reject({ errors: [{ param: "Application", msg: "Cannot parse server response" }] }) });
            }
        }).catch((err) => { reject({ errors: [{ param: "Server", msg: "Cannot communicate" }] }) });
    });
}

async function checkMissingConfiguration(configuration) {
    return new Promise((resolve, reject) => {
        fetch(baseURL + "/noConfiguration", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(configuration),
        }).then((response) => {
            if (response.ok) {
                resolve(null);
            } else {
                response.json()
                    .then((obj) => { reject(obj); })
                    .catch((err) => { reject({ errors: [{ param: "Application", msg: "Cannot parse server response" }] }) });
            }
        }).catch((err) => { reject({ errors: [{ param: "Server", msg: "Cannot communicate" }] }) });
    })
}

const API = { isAuthenticated, createOfficerAccount, getCounters, getServices, getServingTime, createService, getConfigurations, createConfiguration, login, saveNewConfiguration, checkMissingConfiguration, };

export default API;
