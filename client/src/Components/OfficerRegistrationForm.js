import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Redirect } from 'react-router-dom';
import { AuthContext } from '../auth/AuthContext'
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

import OfficerAccount from '../api/OfficerAccount'



class SessionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state.submitted = false;
  }

  updateField = (event) => {
    event.target.setCustomValidity("");
    this.setState({ [event.target.name] : event.target.value });
  }

  //Chekc if the password is valid
  onChangePassword = (event) => {
    event.target.setCustomValidity("");
    if (event.target.value.length >= 8) {
      this.updateField(event);
    } else {
      event.target.setCustomValidity("The password must be at least 8 character long");
    }
  }

  onChangeRepeatPassword = (event) => {
    event.target.setCustomValidity("");
    if (event.target.value === this.state.password) {
      this.updateField(event);
    } else {
      event.target.setCustomValidity("The password doesn't match");
    }
  }


  handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    if (!form.checkValidity()) {
      form.reportValidity();
    } else {
      this.props.createOfficerAccount(new OfficerAccount(this.state.name, this.state.surname, "false" /*is not a manager*/, this.state.email, this.state.password),
      () => { // success callback
        this.setState({ submitted: true });
        window.alert("Officer create successfully!");
      },
      (err) => { // error call back
        //console.log(err);
        //if(err.status == 403)
          document.getElementById("email").setCustomValidity("This e-mail is already used!");
          form.reportValidity();
      }
      );
    }
  }


  render() {
    if (this.state.submitted)
      return <Redirect to='/login' />; // TODO: funzionera quando sarà attivo il login
    return (
      <AuthContext.Consumer>
        {(context) => (
          <>
            {/*(context.authErr || !context.authUser) && <Redirect to="/login"></Redirect>  TODO: quando funzionera il login*/}
            <Container fluid>
              <Row>
                <Col>
                  <h1>Officer registration</h1>
                  <br />
                  <Form method="POST" onSubmit={(event) => this.handleSubmit(event)}>
                  <Form.Group controlId="Officer Name">
                      <Form.Label>Officer Name</Form.Label>
                      <Form.Control type="text" name="name" placeholder="Name" onChange={(ev) => this.updateField(ev)} required/>
                    </Form.Group>

                    <Form.Group controlId="Officer Surname">
                      <Form.Label>Officer Surname</Form.Label>
                      <Form.Control type="text" name="surname" placeholder="Surname" onChange={(ev) => this.updateField(ev)} required />
                    </Form.Group>

                    <Form.Group controlId="email">
                      <Form.Label>Officer E-mail</Form.Label>
                      <Form.Control type="email" name="email" placeholder="E-mail" onChange={(ev) => this.updateField(ev)} required />
                    </Form.Group>

                    <Form.Group controlId="password">
                      <Form.Label>Password</Form.Label>
                      <Form.Control type="password" name="password" placeholder="Password" onChange={(ev) => this.onChangePassword(ev)} required />
                    </Form.Group>

                    <Form.Group controlId="repeat-password">
                      <Form.Label>Repeat password</Form.Label>
                      <Form.Control type="password" name="repeat-password" onChange={(ev) => this.onChangeRepeatPassword(ev)} required />
                    </Form.Group>

                    <Form.Group>
                      <Button variant="primary" type="submit">Register</Button>
                    </Form.Group>
                  </Form>

                </Col>
              </Row>
            </Container>
          </>
        )}
      </AuthContext.Consumer>
    );
  }
}

export default SessionForm;