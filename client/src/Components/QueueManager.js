import React from 'react';
import Alert from "react-bootstrap/Alert";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import API from "../api/API.js";

class QueueManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            counters: [],
            services: [],
            serviceIds: [],
            assignments: [], //array of arrays where each position corresponds to a counter and each internal array corresponds to all services available; assignments[i][j] = true means that the counter identified by i is providing the service identified by j
            showModal: false, //used to have a pop-up when saving the services offered by a counter
            counterSaved: undefined,
            debug: false,
            loaded: false,
        }
    }

    componentDidMount() {
        API.getCounters().then((counters) => {
            console.log(counters);
            //let ctrs = [];
            //counters.map((c) => ctrs.push(c.counterId));
            this.setState({ counters: counters })
        }).then(() => {
            API.getServices().then((services) => {
                let srvs = [];
                let servIds = [];
                services.map((s) => {
                    srvs.push(s.description);
                    servIds.push(s.serviceId);
                });
                this.setState({ services: srvs, serviceIds: servIds })
            });
        }).then(() => {
            API.getConfigurations().then((configs) => {
                if (configs.length === 0) {
                    let configurations = [];
                    for (let i = 0; i < this.state.counters.length; i++) {
                        let boolValues = [];
                        for (let j = 0; j < this.state.services.length; j++) {
                            boolValues.push(false);
                        }
                        configurations[i] = boolValues;
                    }

                    this.setState({ assignments: configurations, loaded: true })
                } else {

                    let configurations = [];
                    for (let i = 0; i < this.state.counters.length; i++) {
                        let currentCounterId = this.state.counters[i];
                        let boolValues = [];
                        for (let j = 0; j < this.state.services.length; j++) { //inizializza tutti i servizi per il counter a false (non forniti)
                            boolValues.push(false);
                        }
                        for (let j = 0; j < configs.length; j++) {

                            let counterId = configs[j].counterID; //prende i singoli campi di ciascuna coppia registrata
                            let serviceId = configs[j].serviceID;
                            if (counterId === currentCounterId) {
                                let servicePos = this.state.serviceIds.indexOf(serviceId);
                                boolValues[servicePos] = true;
                            }
                        }
                        configurations[i] = boolValues;
                    }
                    this.setState({ assignments: configurations, loaded: true })
                }
            })
        });
    }

    createRow = (c) => {
        return (
            <tr>
                <td> Counter Number {c} </td>
                <td> <Table hover={true} bordered size="sm">
                    <thead>
                        <tr>
                            <th> Service Name </th>
                            {this.state.debug && <th> Served By Counter </th>}
                            {this.state.debug && <th> Change Served/Not Served </th>}
                            <th> Served by Counter </th>
                        </tr>
                    </thead>
                    <tbody>{this.state.services.map((s) => this.createInnerRow(s, c))}</tbody>
                </Table></td>
                <td> <Button variant="outline-success" onClick={() => this.confirmServices(c)}> Confirm </Button></td>
            </tr >
        );
    }

    createInnerRow = (s, c) => {
        return (
            <tr>
                <td> {s} </td>
                {this.state.debug && <td> {this.getServed(s, c)} </td>}
                {this.state.debug && <td> <Button variant="outline-info" onClick={() => this.changeService(s, c)}> Change</Button></td>}
                <td> <Form>
                    <Form.Group controlId="formCheckbox">
                        <Form.Check type="checkbox" checked={this.getChecked(s, c)} onChange={() => this.onChangeCheckbox(s, c)}></Form.Check>
                    </Form.Group>
                </Form> </td>
            </tr>
        )
    }

    getChecked = (service, counter) => {
        if (this.state.loaded) {
            let i = this.state.services.indexOf(service);
            let j = this.state.counters.indexOf(counter);
            let counterAssignments = this.state.assignments[j];
            return (counterAssignments[i]);
        } else {
            console.log("LOADING...")
            return false;
        }
    }

    onChangeCheckbox = (service, counter) => {
        let i = this.state.services.indexOf(service);
        let j = this.state.counters.indexOf(counter);
        let assignments = this.state.assignments;
        let counterAssignments = assignments[j]; //checks if the given counter is providing or not the given service
        let assigned = !counterAssignments[i]; //changes the assigned value so that the counter starts/stops providing the service
        counterAssignments[i] = assigned; //updates the array of arrays so that the inner table can properly display the updated value
        assignments[j] = counterAssignments;
        this.setState({ assignments: assignments });
    }

    confirmServices = (counter) => {
        let i = this.state.counters.indexOf(counter);
        let counterAssignments = this.state.assignments[i];
        for (let j = 0; j < counterAssignments.length; j++) {
            if (counterAssignments[j]) { //if there's a configuration for the pair counter-service then the server will check if it's new(then it will be saved) or not(then it won't do anything)
                let serviceId = this.state.serviceIds[j];
                let configuration = {
                    counterId: counter,
                    serviceId: serviceId,
                }
                API.saveNewConfiguration(configuration).then(() => console.log("CREATION OK"))
            } else { //if there's no configuration for the pair the server will check if it already exist (then it will remove the configuration) or not(then it won't do anything)
                let serviceId = this.state.serviceIds[j];
                let configuration = {
                    counterId: counter,
                    serviceId: serviceId,
                }
                API.checkMissingConfiguration(configuration).then(() => console.log("ABSENT CONF OK"))
            }
        }
        this.setState({ showModal: true, counterSaved: counter });
        //counterSaved is used to have a title in the confirmation Modal and to specify the counter for which services have been updated 
    }

    closeModal = () => {
        this.setState({ showModal: false });
    }


    render() {
        return (<>
            <Container fluid>
                <Row>
                    <Col>
                        <h1>Queue Setup</h1>
                        <br />
                        <Table bordered size="sm">
                            <thead>
                                <tr>
                                    <th> Counter </th>
                                    <th> Services </th>
                                    <th> Confirm Services </th>
                                </tr>
                            </thead>
                            <tbody>{this.state.counters.map((c) => this.createRow(c))}</tbody>
                        </Table>
                        <Modal show={this.state.showModal}>
                            <Modal.Header closeButton={true} onClick={() => this.closeModal()}>
                                <Modal.Title> Counter Number {this.state.counterSaved}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Alert variant="success"> Changes saved successfully! </Alert>
                            </Modal.Body>
                        </Modal>
                    </Col>
                </Row>
            </Container>



        </>);
    }
}

export default QueueManager;