# Office Queue Working Hours

## Sprint 1: 16/10/2020 - 22/10/2020

- Giacomo Garaccione
	* 16/10/2020: Set up the front end and the back end: 55min.
	* 17/10/2020: Created admin management page (front-end): 1h 50min.
	* 20/10/2020: Graphical changes in admin management page: 20min
	* 20/10/2020: Server work for queue management page: 50min
  
- Gaetano Epiro 
	* 16/10/2020: Learn how to setup the database: 30min
	* 17/10/2020: Setup the database: 1.25h
	* 19/10/2020: Setup the database: 35min
	* 20/10/2020: Setup the database: 2h
	* 21/10/2020: Learn testing framework mocha and chai: 2.30h
	* 22/10/2020: Testing with mocha and chai: 1h
  
- Daniele Aurigemma
	* 17/10/2020: implemented front-end page for creating officer account and implemented(but not tested) this page back-end. Updated package and App.js: 3h 45min
	* 19/10/2020: Fixed Manager Officer registration page front-end to fit database design: 30m
	* 19/10/2020: Removed Manager email field from officer registration form: 5m
	* 20/10/2020: Fixed some minor issues: 5m
	* 20/10/2020: Implemented unique email check for officer creation: 25m
	* 20/10/2020: Made server and account creation working and fixed some issues: 30m
	* 20/10/2020: Lerned Testing framework and implemented documented test skeleton: 1h 40m
	* 22/10/2020: Test and code review: 1h
	
- Alessandro Lepori
	* 17/10/2020: Setup the database: 1.25h
	* 19/10/2020: Setup the database: 35min
	* 20/10/2020: Setup the database: 2h

- Yenan Gu
    * 18/10/2020: implemented front-end page for login and logout： 2h
	* 20/10/2020: implemented back-end for login and logout page： 3h
	* 21/10/2020: fixed front-end for login and logout page to fit the latest design： 30m
	* 21/10/2020: fixed back-end for login to fit the database design： 30m
- Giuseppe Salemi
	* 16/10/2020: study of Javascript: 1h
	* 17/10/2020: study of Javascript: 2h
	* 18/10/2020: study of React and Bootstrap: 2h
	* 19/10/2020: study of React and Bootstrap: 1h
	* 20/10/2020: study of React and Bootstrap: 1h
	* 21/10/2020: study of Node.js: 1h